var joi = require('joi');
var good = require('good');

//---------------------------------------------Création du serveur
var hapi = require('hapi');
var server = new hapi.Server();

//---------------------------------------------Définition de la connexion
server.connection({
	host:'localhost',
	port: Number(process.argv[2] || 8080)
});

server.start(function(){
	console.log('Server running at', server.info.uri);
});

//---------------------------------------------Définition des routes
server.route({path:'/', method:'GET', handler: rootHandler});
function rootHandler(request,reply){
	reply('API Hapi-ness');
};

server.route({path:'/crew',
 method:'GET', 
 config: {
	handler: getCrew,
		validate:{
			query:{
				name: joi.string().min(4).max(25),
				callback: joi.string()
			}
		},
	jsonp: 'callback'
	} 
});

server.route({
    method: 'GET',
    path: '/crew/{id}',
    config: {
        handler: getMember,
        jsonp: 'callback',
        description: 'This method will return a specific crew member, if it exists.',
        notes: 'The id parameter is a required numeric value.',
        tags: ['api', 'crew']
    }
});

server.route({
    method: 'POST',
    path: '/crew',
    config: {
        handler: addMember,
        validate: {
            payload: {
                name: joi.string().required().min(3)
            }
        }
    }
});

//---------------------------------------------Conception de la réponse
function getCrew(request, reply){
//---------------------------------------------Accession aux paramètres URL
var crewResponse = crewDetails;
if(request.query.name)
	crewResponse = findCrewMember(request.query.name);
	reply({
		status: 200,
		request:{
			made: request.info.received,
			params: request.query
		},
		response:{
			count: crewResponse.length,
			crew: crewResponse
		}
	});
}

//---------------------------------------------Trouver un membre
function getMember(request, reply) {
  var member = crewDetails.filter(function(member) {
      return member.id === parseInt(request.params.id);
  }).pop();
  if (typeof(member) !== 'object') member = {};
  reply({
    status: 200,
    request: {
      made: request.info.received,
      params: request.params
    },
    response: {
      crew_member: member
    }
  });
}

//---------------------------------------------Ajouter un membre
function addMember(request, reply) {
    var member = {
        id: crewDetails[crewDetails.length - 1].id + 1,
        name: request.payload.name
    };
    crewDetails.push(member);
    reply(member).code(201).header('Location', '/crew/' + member.id);
}


//---------------------------------------------Supprimer un membre
function deleteMember(request, reply) {
  var member;
  for ( var i = 0; i < crewDetails.length; i++ ) {
    if ( crewDetails[i].id === parseInt(request.params.id) ) {
      member = crewDetails[i];
      crewDetails.splice(i, 1);
      reply(member).code(200).header('Location', '/crew/' + member.id);
      break;
    }
  }
}

//---------------------------------------------Structure des données
var crewDetails = [{
		id:1,
		name:'LaCrotteDePoule',
	},
	{
		id:2,
		name:'LeChienQuiFume',
	}
]

//---------------------------------------------Données filtre
function findCrewMember(name){
	return crewDetails.filter(function(member)
	{
		return member.name.toLowerCase() === name.toLowerCase();
	});
}

//---------------------------------------------Souscription au plugin
server.register([{
	register: good,
	options:{
		reporters:[{
			reporter: require('good-console'),
			events: {response:'*', log:'*'}
		}]
	}
}],
	function(err){
		if (err) throw err;
		server.start(function(){
			server.log('info','server running at:' + server.info.uri)
		});
	}
)